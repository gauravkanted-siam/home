
# Login
This is backend login module. here we are authenticating the user based on Email and password

# Entities

	Email field: type = email
	
	Password field: type = password

# Steps

 - After getting the input information/data through form submit or API from front end we have to parse and validate the given fields are valid as per the defined rules

	 - If it’s not we have to send error message as defined in rules or through the field missing or required field error message

 - If the required data all passed to the backend then we have to check whether the given email id is already present in Auth Table.

	 - If email is present then we have to encode the password and check it with the encoded password of the record in DB. And we have to do business case checks to validate whether the user is active or not.

	- If the email is not present we have to send the response with not exists message and stop the process

	 - If both username and password does not match, we have to through the invalid credentials message to the frontend and stop the process

# Pseudo Code

	Get Request Inputs [username/email,password]

	Parse Input

	Validate the inputs that meet the requirements

	IF (DBLoginID == EnteredUsername)

		Encode Password

	IF (DBPassword == EncodedEnteredPassword) THEN

		Print/Response Logged in success with 200 Status Code

		Send token and user details if its API

	Else

		Print/Response “Invalid Password” with 403 Status Code

	Else

		Print/Response “Username not found” with 403 Status Code