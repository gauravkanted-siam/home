# Introduction
Here we are listing and collaborating list of backend common module, which we can use as a common repository for various purpose. 

**Table of Contents** 

- [Login](backend/login.md)
- [Open Registration](backend/open_registration.md)
- [Email Invite Based Registration](backend/email_invite_based_registeration.md)
