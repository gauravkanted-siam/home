# Introduction
Before start writing program, We need to know what the program supposed to do. Without that we couldn’t write code in any language. It will be differ per person by person. Some peoples used to write it in notepad, some peoples think in their mind and start code. But there is a chance where every one might miss some scenarios in some places. It is a good idea to write the program flow in a simple way that can be understandable by everyone (developers / business analyst) to ensure you have included everything you need. The best way to achieve this is by using pseudo code.

# Pseudo Code
Pseudo Code is way of writing the program in a simple way. Pseudo code is a non-formal language and does’t have standard, people use this to write a program in any language. Before writing a program you need to know what the program will do and how it will function, then we can use pseudocode to achieve the required results for your program. 
Some of us may think that using pseudo-code to design the application instead of creating and deploying the application is a waste of time. This is true for small modules, that were created many times and which require just some small modifications to accomplish a specific task, but when it comes to large projects, it’s pretty easy to be lost in the hundreds or even thousands of code lines. In this case, the pseudo-code clearly describes the application prototype, so these can be easily implemented and it gives the developer an opportunity to think on the algorithm before implementing it.


## Sample Structure 
    Application Start
    Initialise/Declare variables

    . . . 
    Statements
    Assign or modify the variables
    Return if anything needed
    . . . 

    Application End

## Sample Pseudo Code
Write a program for adding two numbers

    Application Start
    Prompt for input A and B
    Enter two numbers, A, B
    Add the numbers and Assign it to Sum variable
    Print Sum
    Application End 

From the above example we have to remember, writing pseudo code is not like writing an actual programming language. It cannot be interpreted/compiled or run like a regular programming languages, it can be written how you want. But as a organisation we have to maintain some sort of standards which is understandable to all of us. 

Some of us may think that using pseudo-code to design the application instead of creating and deploying the application is a waste of time. This is true for small modules, that were created many times and which require just some small modifications to accomplish a specific task, but when it comes to large projects, it’s pretty easy to be lost in the hundreds or even thousands of code lines. In this case, the pseudo-code clearly describes the application prototype, so these can be easily implemented and it gives the developer an opportunity to think on the algorithm before implementing it.

