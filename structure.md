# Directory Structure

Siam Computing uses a High level directory structure Based on divisions. All Frontend files listed in the [Frontend](frontend/README.md). backend files and folders are comes under [backend](backend/README.md). Other modules are present in respective folders of their own.

Our basic high level structure looks something like this:

```
.
├── book.json
├── README.md
├── SUMMARY.md
├── frontend/
|   ├── README.md
|   └── module1.md
|── backend/
|    ├── README.md
|    └── module1.md
├── mobile/
|   ├── README.md
|   └── module1.md
├── server/
|   ├── README.md
|   └── module1.md
└── mlai/
    ├── README.md
    └── module1.md

```

An overview of what each of these does:

| File | Description |
| -------- | ----------- |
| `book.json` | Core Config for documentation purpose (**required**) |
| `README.md` | Root level Introduction for our documentation (**required**) |
| `SUMMARY.md` | Table of Contents [SUMMARY](SUMMARY.md) (**required**) |
| `Frontend` | For Frontend modules. It will have its own Table of Index [Frontend](frontend/README.md) (**required**) |
| `Backend` | For Backend modules. It will have its own Table of Index [Backend](backend/README.md) (**required**) |
| `Mobile` | For Mobile modules. It will have its own Table of Index [Mobile](mobile/README.md) (**required**) |
| `Server` | For Server modules. It will have its own Table of Index [Server](server/README.md) (**required**) |
| `ML/AI` | For Frontend modules. It will have its own Table of Index [ML/AI](mlai/README.md) (**required**) |


### Ignoring files & folders {#ignore}

We have to use will read the `.gitignore`, `.bookignore` and `.ignore` files to get a list of files and folders to skip.
The format inside those files, follows the same convention as `.gitignore`:

```
# This is a comment

# Ignore the file login-module-old.md
login-module-old.md

# Ignore everything in the directory "signup"
signup/*
```
